import Vue from 'vue'
import Router from 'vue-router'
import Liste from './pages/frontOffice/liste/liste';
import Carte from './pages/frontOffice/carte/carte';
import Informations from './pages/frontOffice/informations/informations';
import Configuration from './pages/backOffice/configuration/configuration.vue';
import Ruchers from './pages/backOffice/ruchers/ruchers';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'accueil',
            component: Liste,
        },
        {
            path: '/liste',
            name: 'liste',
            component: Liste,
        },
        {
            path: '/carte',
            name: 'carte',
            component: Carte,
        },
        {
            path: '/informations',
            name: 'informations',
            component: Informations,
        },
        {
            path: '/configuration',
            name: 'configuration',
            component: Configuration,
        },
        {
            path: '/ruchers',
            name: 'ruchers',
            component: Ruchers,
        },
    ],
})
