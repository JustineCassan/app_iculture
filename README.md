# app'iculture

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Model
```
See https://www.figma.com/file/XuBjlxjfWhmWFS7Dwt8ufGgt/Untitled?node-id=0%3A1
```
### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
